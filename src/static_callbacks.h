#ifndef STATIC_CALLBACKS_H
#define STATIC_CALLBACKS_H
//
// Created by khatharsis on 11/11/22.
//
#include <otf2/otf2.h>
#include <otf2/OTF2_GeneralDefinitions.h>
#include "otf2_struct.h"
#include <iterator>
#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stack>
#include <algorithm>
#include "magic_enum.hpp"

static std::ostream *dbg_stream;
static std::ostream *def_stream;
static std::ostream *evt_stream;

#define DBG_PRINT(value) *dbg_stream << ((dbg_stream == &std::cout) ? "\033[1;31m[DBG]\033[0;31m - " : "[DBG] - ") \
    << value << ((dbg_stream == &std::cout) ? "\033[0m":"") << "\n"

#define DEF_PRINT(value) *def_stream << ((def_stream == &std::cout) ? "\033[1;32m[DEF]\033[0;32m - " : "[DEF] - " ) \
    << value << ((def_stream == &std::cout) ? "\033[0m":"") << "\n"
#define EVT_PRINT(value) *evt_stream << ((evt_stream == &std::cout) ? "\033[1;35m[EVT]\033[0;35m - " : "[EVT] - ") \
    << value << ((evt_stream == &std::cout) ? "\033[0m":"") << "\n"

std::string FormatWithCommas(uint64_t value, std::string_view unit="") {
    auto s = std::to_string(value);
    //FIXME We have to use an int rather than a size_t
    //      Because we need it to be signed
    //      This isn't the best idea, since casting is implementation-defined.
    //      However, this code works for all the number we need it to work with.
    int n = s.length() - 3;
    while (n > 0) {
        s.insert(n, " ");
        n -= 3;
    }

    s += " ";
    s += unit;
    return s;
}

#define getUnit(x) (0 <= x && x < 10'000) ? 1 : \
        (10'000 <= x && x <= 10'000'000) ? 1'000 : \
        (10'000'000 <= x && x <= 10'000'000'000) ? 1'000'000 : 1'000'000'000

#define getStringUnit(x) (x==1) ? "ns" : (x==1'000) ? "µs" : (x==1'000'000) ? "ms" : "s"

struct Region_stats {
  int nb_calls;
  OTF2_TimeStamp min_duration;
  OTF2_TimeStamp max_duration;
  OTF2_TimeStamp total_duration;
  OTF2_TimeStamp total_duration_excl;
};

struct ThreadData {
    /** Time spent in OMP Barriers. */
    uint64_t omp_barrier_time = 0;
    /** Time spent in MPI Functions. */
    uint64_t mpi_time = 0;
    /** Time spent in I/O Functions. */
    uint64_t io_time = 0;
    /** Time spent in other Functions. */
    uint64_t other_time = 0;

  /** Timestamp of the first event. */
  OTF2_TimeStamp first_timestamp = 0; // TODO: get the date of the thread_begin event

  /** Timestamp of the last event. */
  OTF2_TimeStamp last_timestamp = 0;  // TODO: get the date of the thread_end event

  /** CPU time of the thread. */
  uint64_t cpu_time = 0;



  std::map<OTF2_RegionRef, struct Region_stats>  region_stats;

  void updateStats(OTF2_RegionRef region, OTF2_TimeStamp duration, OTF2_TimeStamp duration_excl) {
    if(region_stats[region].nb_calls == 0) {
      region_stats[region].min_duration = duration;
      region_stats[region].max_duration = duration;
      region_stats[region].total_duration = duration;
      region_stats[region].total_duration_excl = duration_excl;
      region_stats[region].nb_calls = 1;
    } else {
      if(duration < region_stats[region].min_duration) region_stats[region].min_duration = duration;
      if(duration > region_stats[region].max_duration) region_stats[region].max_duration = duration;
      region_stats[region].total_duration += duration;
      region_stats[region].total_duration_excl += duration_excl;
      region_stats[region].nb_calls++;
    }

    assert(region_stats[region].total_duration_excl <= cpu_time);
  }

  /** Time spent in all the Functions */
  [[nodiscard]] uint64_t total_time() const {
    return last_timestamp - first_timestamp;
  }

  // find a regionRef similar (ie. same function name) to ref in the current ThreadData
private:
  std::vector<std::pair<OTF2_RegionRef, struct Region_stats> > sortRegionStats() const {
    std::vector<std::pair<OTF2_RegionRef, struct Region_stats> > result(region_stats.begin(), region_stats.end());

    std::sort(result.begin(),
	      result.end(),
	      [](const std::pair<OTF2_RegionRef, struct Region_stats> a,
		 const std::pair<OTF2_RegionRef, struct Region_stats> b) ->bool
	      { return a.second.total_duration > b.second.total_duration; }
	      );
    return result;
  }

  OTF2_RegionRef mergeRegion(OTF2_RegionRef ref) const {
    static std::map<OTF2_RegionRef, OTF2_RegionRef> matchingRegions;

    if(matchingRegions.count(ref) > 0) // ref is already known
      return matchingRegions[ref];

    // ref not known. Search for a ref with the same name
    if(region_stats.size()> 0) {
      for(auto r:region_stats) {
	if(regionMap[r.first].name.compare(regionMap[ref].name) == 0) {
	  // found two functions with the same name (ref and r.first)
	  if(matchingRegions.count(r.first) > 0) {
	    // r.first is already known. Make sure we use the same ref
	    matchingRegions[ref] = matchingRegions[r.first];
	    return matchingRegions[ref];
	  }

	  // register both r.first and ref
	  matchingRegions[ref] = r.first;
	  matchingRegions[r.first] = r.first;
	  return r.first;
	}
      }
    }

    // ref is the only with with this name
    matchingRegions[ref] = ref;
    return ref;
  }

public:
  void operator+=(const struct ThreadData otherThreadData) {
        omp_barrier_time += otherThreadData.omp_barrier_time;
        mpi_time += otherThreadData.mpi_time;
        io_time += otherThreadData.io_time;
        other_time += otherThreadData.other_time;

	if(cpu_time==0) {
	  first_timestamp = otherThreadData.first_timestamp;
	  last_timestamp = otherThreadData.last_timestamp;
	} else {
	  first_timestamp = std::min(first_timestamp, otherThreadData.first_timestamp);
	  last_timestamp = std::max(last_timestamp, otherThreadData.last_timestamp);
	}
	cpu_time += otherThreadData.cpu_time;

	for(auto r:otherThreadData.region_stats) {
	  OTF2_RegionRef ref = mergeRegion(r.first);

	  region_stats[ref].total_duration_excl += r.second.total_duration_excl;
	  region_stats[ref].total_duration += r.second.total_duration;
	  if(region_stats[ref].nb_calls == 0 ||
	     region_stats[ref].min_duration > r.second.min_duration)
	    region_stats[ref].min_duration = r.second.min_duration;

	  if(region_stats[ref].nb_calls == 0 ||
	     region_stats[ref].max_duration < r.second.max_duration)
	    region_stats[ref].max_duration = r.second.max_duration;

	  region_stats[ref].nb_calls += r.second.nb_calls;
	}
    }

    /** Prints the data */
    void print(const std::string &prefix = "") const {
        const uint64_t unit = getUnit(std::min(
                std::min((omp_barrier_time == 0) ? UINT64_MAX : omp_barrier_time,
                         (mpi_time == 0) ? UINT64_MAX : mpi_time),
                std::min((io_time == 0) ? UINT64_MAX : io_time,
                         (other_time == 0) ? UINT64_MAX : other_time)
        ));
        const std::string unitString = getStringUnit(unit);

        //FIXME Don't cast uint to int.
        int maxSize = FormatWithCommas(
                std::max(
                        std::max(omp_barrier_time, mpi_time),
                        std::max(io_time, other_time)
                ) / unit
        ).size();
	if(!cpu_time)
	  return;
	std::cout << prefix <<"   - CPU time: "
		  << FormatWithCommas(cpu_time/unit)
		  << unitString << "\n";

	std::cout << prefix <<"   - first_timestamp: " << first_timestamp<<"\n";
	std::cout << prefix <<"   - last_timestamp: " << last_timestamp<<"\n";

        if (omp_barrier_time)
            std::cout << prefix << "   - Time spent inside OMP Barriers:    "
                      << std::setw(maxSize) << FormatWithCommas(omp_barrier_time / unit)
                      << unitString << " (" << (100 * omp_barrier_time / cpu_time) << "%)" << '\n';
        if (mpi_time)
            std::cout << prefix << "   - Time spent inside MPI Functions:   "
                      << std::setw(maxSize) << FormatWithCommas(mpi_time / unit)
                      << unitString << " (" << (100 * mpi_time / cpu_time) << "%)" << '\n';
        if (io_time)
            std::cout << prefix << "   - Time spent inside I/O Functions:   "
                      << std::setw(maxSize) << FormatWithCommas(io_time / unit)
                      << unitString << " (" << (100 * io_time / cpu_time) << "%)" << '\n';
        std::cout << prefix << "   - Time spent inside Other Functions: "
                  << std::setw(maxSize) << FormatWithCommas(other_time / unit)
                  << unitString << " (" << (100 * other_time / cpu_time) << "%)" << std::endl;

    }

  void printFunctions(const std::string &prefix = "") const {
	std::vector<std::pair<OTF2_RegionRef, struct Region_stats> > regions = sortRegionStats();

	const uint64_t f_unit = 1'000'000;
	const char* f_unitString = getStringUnit(f_unit);
	std::cout << prefix << "   - Summary of function calls "<< std::endl;

	printf("%s     ", prefix.c_str());
	printf("%-20s", "#Function");
	printf("%20s", "nb_calls");
	printf("%18s", "tot_duration");
	printf("%18s", "tot_duration(%)");
	printf("%15s", "min_duration");
	printf("%15s", "max_duration");
	printf("%15s", "avg_duration");
	printf("\n");

	for(auto r : regions) {
	  double percent = 1; // TODO: compute the percentage of the execution
	  double avg_duration = (double)r.second.total_duration/r.second.nb_calls;
	  printf("%s     ", prefix.c_str());
	  printf("%-20s", regionMap[r.first].name.c_str());
	  printf("%20d", r.second.nb_calls);
	  printf("%15.3f %s", (double)r.second.total_duration_excl/f_unit, f_unitString);
	  printf("%18.3f", 100*((double)r.second.total_duration_excl/cpu_time));
	  printf("%12.3f %s", (double)r.second.min_duration/f_unit, f_unitString);
	  printf("%12.3f %s", (double)r.second.max_duration/f_unit, f_unitString);
	  printf("%12.3f %s", (double)avg_duration/f_unit, f_unitString);
	  printf("\n");
	}
    }
};


struct UserData {
    std::vector<uint64_t> *locations;
    /** Number of threads/MPI processes.*/
    uint64_t nbr_processes;
    /** Map of the threads to their data.*/
    std::map<OTF2_LocationRef, ThreadData> thread_info;
};

// Global Definitions Callbacks
static OTF2_CallbackCode
DefReaderString(void *userData,
                OTF2_StringRef self,
                const char *string) {
    DEF_PRINT("Creating string " << self << ": \"" << string << "\"");
    stringMap[self] = string;
    return OTF2_CALLBACK_SUCCESS;
}

// Set Region Callback
static OTF2_CallbackCode
DefReaderRegion(void *userData,
                OTF2_RegionRef self,
                OTF2_StringRef name,
                OTF2_StringRef canonicalName,
                OTF2_StringRef description,
                OTF2_RegionRole regionRole,
                OTF2_Paradigm paradigm,
                OTF2_RegionFlag regionFlags,
                OTF2_StringRef sourceFile,
                uint32_t beginLineNumber,
                uint32_t endLineNumber) {
    DEF_PRINT("Creating region "
                      << self << ": {"
                      << "name:" << '"' << stringMap[name] << '"'
                      << ((canonicalName != name) ? (", cName:\"" + stringMap[canonicalName] + '"') : "")
                      << ((description != name) ? (", description:\"" + stringMap[description] + '"') : "")
                      << ", role:" << magic_enum::enum_name((OTF2_RegionRole_enum) regionRole)
                      << ", paradigm:" << magic_enum::enum_name((OTF2_Paradigm_enum) paradigm)
                      << ", flags:" << magic_enum::enum_name((OTF2_RegionFlag_enum) regionFlags)
                      << ", source:" << '"' << stringMap[sourceFile] << '"'
                      << ", range:" << beginLineNumber << '-' << endLineNumber
                      << "}");
    regionMap[self] = Region{
            stringMap[name],
            stringMap[canonicalName],
            stringMap[description],
            regionRole,
            paradigm,
            regionFlags,
            stringMap[sourceFile],
            beginLineNumber,
            endLineNumber
    };
    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode
DefReaderSystemTreeNode(void *userData,
                        OTF2_SystemTreeNodeRef self,
                        OTF2_StringRef name,
                        OTF2_StringRef className,
                        OTF2_SystemTreeNodeRef parent) {
    DEF_PRINT("Creating SystemTreeNode "
                      << self << ": {"
                      << "name:" << '"' << stringMap[name] << '"'
                      << ", className:" << '"' << stringMap[className] << '"'
                      << ((parent != UINT32_MAX) ? (", parent:" + std::to_string(parent)) : "")
                      << '}');
    systemTreeNodeMap[self] = SystemTreeNode{
            stringMap[name],
            stringMap[className],
            &systemTreeNodeMap[parent]
    };
    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode
DefReaderLocationGroup(void *userData,
                       OTF2_LocationGroupRef locationGroup,
                       OTF2_StringRef name,
                       OTF2_LocationGroupType locationGroupType,
                       OTF2_SystemTreeNodeRef systemTreeParent
#if OTF2_VERSION_MAJOR >= 3
        , OTF2_LocationGroupRef creatingLocationGroup
#endif
) {
    DEF_PRINT("Creating location group "
                      << locationGroup << ": {"
                      << "name:" << '"' << stringMap[name] << '"'
                      << ", type:" << magic_enum::enum_name((OTF2_LocationGroupType_enum) locationGroupType)
                      << ", parent:" << systemTreeParent
#if OTF2_VERSION_MAJOR >= 3
                      << ((creatingLocationGroup != UINT32_MAX) ? (", creatingLocationGroup:" + locationGroupMap[creatingLocationGroup].name) : "")
#endif
	      << '}');
    locationGroupMap[locationGroup] = LocationGroup{
      locationGroup,
      stringMap[name],
      locationGroupType,
      &systemTreeNodeMap[systemTreeParent]
#if OTF2_VERSION_MAJOR >= 3
      , &locationGroupMap[creatingLocationGroup]
#endif
    };
    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode
DefReaderLocation(void *userData,
                  OTF2_LocationRef location,
                  OTF2_StringRef name,
                  OTF2_LocationType locationType,
                  uint64_t numberOfEvents,
                  OTF2_LocationGroupRef locationGroup) {
    DEF_PRINT("Creating location "
                      << location << ": {"
                      << "name:" << '"' << stringMap[name] << '"'
                      << ", locationType:" << magic_enum::enum_name((OTF2_LocationType_enum) locationType)
                      << ", numberOfEvents:" << numberOfEvents
                      << "}");
    locationMap[location] = Location{
      location,
      stringMap[name],
      locationType,
      numberOfEvents,
      &locationGroupMap[locationGroup]
    };
    ((UserData *) userData)->locations->push_back(location);
    return OTF2_CALLBACK_SUCCESS;
}

// Set Attributes Callback
static OTF2_CallbackCode
DefReaderAttribute(void *userData,
                   OTF2_AttributeRef self,
                   OTF2_StringRef name,
                   OTF2_StringRef description,
                   OTF2_Type type) {
    DEF_PRINT("Creating attribute "
                      << self << ": {"
                      << "name:" << '"' << stringMap[name] << '"'
                      << ((description != name) ? (", description:\"" + stringMap[description] + '"') : "")
                      << ", type:" << magic_enum::enum_name((OTF2_Type_enum) type)
                      << '}');
    attributeMap[self] = Attribute{
            stringMap[name],
            stringMap[description],
            type
    };
    return OTF2_CALLBACK_SUCCESS;
}


// timestamp of the last event in a callstack
static std::map<OTF2_LocationRef, std::stack<OTF2_TimeStamp>> callstack;
// time spent in subfunctions
static std::map<OTF2_LocationRef, std::stack<OTF2_TimeStamp>> inner_duration;
//static std::map<OTF2_LocationRef, std::map<OTF2_RegionRef, OTF2_TimeStamp>> timestampMap;
static std::map<OTF2_LocationRef, std::map<OTF2_RegionRef, struct Region_stats>> region_stats;

static OTF2_CallbackCode
EvtReaderEnter(OTF2_LocationRef location,
               OTF2_TimeStamp time,
               void *userData,
               OTF2_AttributeList *attributes,
               OTF2_RegionRef region) {
    EVT_PRINT("Entering region " << region
                                 << " (" << regionMap[region].canonicalName
                                 << ") at location " << location
                                 << " using paradigm " << unsigned(regionMap[region].paradigm));

    auto castUserData = (UserData *) userData;
    if(! castUserData->thread_info[location].first_timestamp)
      castUserData->thread_info[location].first_timestamp = time;
    callstack[location].push(time);
    inner_duration[location].push(0);
    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode
local_EvtReaderEnter(OTF2_LocationRef location,
		     OTF2_TimeStamp time,
		     uint64_t eventPosition,
		     void *userData,
		     OTF2_AttributeList *attributes,
		     OTF2_RegionRef region) {
  return EvtReaderEnter(location, time, userData, attributes, region);
}

static OTF2_CallbackCode
EvtReaderLeave(OTF2_LocationRef location,
               OTF2_TimeStamp time,
               void *userData,
               OTF2_AttributeList *attributes,
               OTF2_RegionRef region) {
    EVT_PRINT("Leaving region " << region << " at location " << location);
    std::string name = regionMap[region].canonicalName;
    auto castUserData = (UserData *) userData;
    OTF2_TimeStamp enterTimestamp =callstack[location].top();
    callstack[location].pop();
    OTF2_TimeStamp subfunctions_duration = inner_duration[location].top();
    inner_duration[location].pop();

    // duration of the function and its subfunctions
    auto including_duration = time - enterTimestamp;
    // duration of the function only (without accounting its subfunctions)
    auto excluding_duration = including_duration - subfunctions_duration;

    // update the function that called the ending function
    if(!inner_duration[location].empty())
      inner_duration[location].top() += including_duration;


    //   auto timeSpent = time - enterTimestamp;

    castUserData->thread_info[location].last_timestamp = time;
    castUserData->thread_info[location].cpu_time = castUserData->thread_info[location].total_time();
    castUserData->thread_info[location].updateStats(region, including_duration, excluding_duration);

    if (name == "OpenMP implicit barrier" || name == "OpenMP barrier") {
        castUserData->thread_info[location].omp_barrier_time += including_duration;
    } else if (name.find("MPI_") != std::string::npos || name.find("mpi_") != std::string::npos) {
        castUserData->thread_info[location].mpi_time += including_duration;
    } else if (name.find("pthread") != std::string::npos || name.find("sem") != std::string::npos) {
        castUserData->thread_info[location].io_time += including_duration;
    } else {
        castUserData->thread_info[location].other_time += excluding_duration;
    }
    return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode
local_EvtReaderLeave(OTF2_LocationRef location,
               OTF2_TimeStamp time,
	       uint64_t eventPosition,
               void *userData,
               OTF2_AttributeList *attributes,
               OTF2_RegionRef region) {
  return EvtReaderLeave(location, time, userData, attributes,region);
}


#endif	/* STATIC_CALLBACKS_H */
