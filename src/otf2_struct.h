//
// Created by khatharsis on 11/11/22.
//
#include <otf2/otf2.h>
#include <otf2/OTF2_GeneralDefinitions.h>
#include <iterator>
#include <map>
#include <iostream>
#include <string>
// https://perftools.pages.jsc.fz-juelich.de/cicd/otf2/tags/otf2-3.0/html/group__records__definition.html
/**
 * Defines the timer resolution and time range of this trace.
 * There will be no event with a timestamp less than globalOffset,
 * and no event with timestamp greater than (globalOffset + traceLength).
 *
 * This definition is only valid as a global definition.
 */
struct ClockProperties {
    /**
     * Ticks per seconds.
     */
    uint64_t timerResolution;
    /**
     * A timestamp smaller than all event timestamps.
     */
    uint64_t globalOffset;
    /**
     * A timespan which includes the timespan between the smallest
     * and greatest timestamp of all event timestamps.
     */
    uint64_t traceLength;
#if OTF2_VERSION_MAJOR >= 3
    /**
     * A realtime timestamp of the globalOffset timestamp in nanoseconds since 1970-01-01T00:00 UTC.
     * Use OTF2_UNDEFINED_TIMESTAMP if no such timestamp exists.
     */
    uint64_t realtimeTimestamp;
#endif
};

/**
 * Attests that the following parallel paradigm was available
 * at the time when the trace was recorded, and vice versa.
 * Note that this does not attest that the paradigm was used.
 * For convenience, this also includes a proper name for the paradigm and a classification.
 * This definition is only allowed to appear at most once in the definitions per Paradigm.
 * This definition is only valid as a global definition.
 */
struct Paradigm {
    /**
     * The paradigm to attest.
     */
    OTF2_Paradigm paradigm;
    /**
     * The name of the paradigm.
     */
    std::string name;
    /**
     * The class of this paradigm.
     */
    OTF2_ParadigmClass paradigmClass;
};

/**
 * Extensible annotation for the Paradigm definition.
 *
 * The tuple (paradigm, property) must be unique.
 *
 * This definition is only valid as a global definition.
 */
struct ParadigmProperty {
    /**
     * The paradigm to annotate.
     */
    OTF2_Paradigm paradigm;
    /**
     * The property.
     */
    OTF2_ParadigmProperty property;
    /**
     * The type of this property.
     * Must match with the defined type of the property.
     */
    OTF2_Type type;
    /**
     * The value of this property.
     */
    OTF2_AttributeValue value;
};

/**
 * Attests that the following I/O paradigm was available at the time when the trace was recorded, and vice versa. Note that this does not attest that the paradigm was used. For convenience, this also includes a proper name for the paradigm and a classification.
 *
 * This definition is only valid as a global definition.
 */
struct IoParadigm {
    /**
     * The I/O paradigm identification. This should be used programmatically to identify a specific I/O paradigm. For a human-readable name use the name attribute. If this identification matches one of the known I/O paradigms listed in the OTF2 documentation Known OTF2 I/O paradigms, then the attributes of this definition must match those specified there. References a String definition.
     */
    std::string identification;
    /**
     * The name of the I/O paradigm. This should be presented to humans as the name of this I/O paradigm. References a String definition.
     */
    std::string name;
    /**
     * The class of this I/O paradigm.
     */
    OTF2_IoParadigmClass ioParadigmClass;
    /**
     * Boolean properties of this I/O paradigm.
     */
    OTF2_IoParadigmFlag ioParadigmFlags;
    /**
     * Number of properties.
     */
    uint8_t numberOfProperties;
    /**
     * The properties.
     */
    const OTF2_IoParadigmProperty *properties;
    /**
     * The types of these properties. Must match with the defined type of the property.
     */
    const OTF2_Type *types;
    /**
     * The values of these properties.
     */
    const OTF2_AttributeValue *values;
};
/**
 * Map of the I/O Paradigms.
 */
static std::map<OTF2_IoParadigmRef, struct IoParadigm> ioParadigmMap;
// TODO Utiliser ça pour créer les maps
/**
 * Mapping tables are needed for situations where an ID is not globally known at measurement time. They are applied automatically at reading.
 *
 * This definition is only valid as a local definition.
 */
struct MappingTable {
    /**
     * Says to what type of ID the mapping table has to be applied.
     */
    OTF2_MappingType mappingType;
    /**
     * Mapping table.
     */
    const OTF2_IdMap *idMap;
};

/**
 * Clock offsets are used for clock corrections.
 *
 * This definition is only valid as a local definition.
 */
struct ClockOffset {
    /**
     * Time when this offset was determined.
     */
    OTF2_TimeStamp time;
    /**
     * The offset to the global clock which was determined at time.
     */
    int64_t offset;
    /**
     * A possible standard deviation, which can be used as a metric for the quality of the offset.
     */
    double standardDeviation;
};

/**
 * Map of the strings.
 */
static std::map<OTF2_StringRef, std::string> stringMap;


/**
 * The attribute definition.
 */
struct Attribute {
    /**
     * Name of the attribute.
     */
    std::string name;
    /**
     * Description of the attribute.
     */
    std::string description;
    /**
     * Type of the attribute value.
     */
    OTF2_Type type;
};

/**
 * Map of the Attributes.
 */
static std::map<OTF2_AttributeRef, struct Attribute> attributeMap;

/**
 * The system tree node definition.
 */
struct SystemTreeNode {
    /**
     * Free form instance name of this node.
     */
    std::string name;
    /**
     * Free form class name of this node.
     */
    std::string className;
    /**
     * Parent id of this node. May be nullptr to indicate that there is no parent.
     */
    SystemTreeNode *parent;
};
/**
 * Map of the SystemTreeNode.
 */
static std::map<OTF2_SystemTreeNodeRef, struct SystemTreeNode> systemTreeNodeMap;

/**
 * The location group definition.
 */
struct LocationGroup {
  OTF2_LocationGroupRef id;
    /**
     * Name of the group. References a String definition.
     */
    std::string name;
    /**
     * Type of this group.
     */
    OTF2_LocationGroupType groupType;
    /**
     * Parent of this location group in the system tree.
     */
    SystemTreeNode *systemTreeParent;
#if OTF2_VERSION_MAJOR >= 3
    /**
     * The creating location group of this group.
     * For type OTF2_LOCATION_GROUP_TYPE_PROCESS
     * this may be another group of type OTF2_LOCATION_GROUP_TYPE_PROCESS or OTF2_UNDEFINED_LOCATION_GROUP.
     * For type OTF2_LOCATION_GROUP_TYPE_ACCELERATOR,
     * this must be a group of type OTF2_LOCATION_GROUP_TYPE_PROCESS.
     * References a LocationGroup definition.
     */
    LocationGroup* creatingLocationGroup;
#endif
};
/**
 * Map of the locationGroup.
 */
static std::map<OTF2_LocationGroupRef, struct LocationGroup> locationGroupMap;

/**
 * The location defintion.
 */
struct Location {
  OTF2_LocationRef id;
  /**
     * Name of the location.
     */
    std::string name;
    /**
     * Location type.
     */
    OTF2_LocationType locationType;
    /**
     * Number of events this location has recorded.
     */
    uint64_t numberOfEvents;
    /**
     * Location group which includes this location.
     */
    LocationGroup *locationGroup;
};
/**
 * Map of the locations.
 */
static std::map<OTF2_LocationRef, struct Location> locationMap;

struct Region {
    /**
     * Name of the region (demangled name if available).
     */
    std::string name;
    /**
     * Alternative name of the region (e.g. mangled name).
     */
    std::string canonicalName;
    /**
     * A more detailed description of this region.
     */
    std::string description;
    /**
     * Region role.
     */
    OTF2_RegionRole regionRole;
    /**
     * Paradigm.
     */
    OTF2_Paradigm paradigm;
    /**
     * Region flags.
     */
    OTF2_RegionFlag regionFlags;
    /**
     * The source file where this region was declared.
     */
    std::string sourceFile;
    /**
     * Starting line number of this region in the source file.
     */
    uint32_t beginLineNumber;
    /**
     * Ending line number of this region in the source file.
     */
    uint32_t endLineNumber;
};
/**
 * Map of the regions.
 */
static std::map<OTF2_RegionRef, struct Region> regionMap;

struct CallSite {
    /**
     * The source file where this call was made.
     */
    std::string sourceFile;
    /**
     * Line number in the source file where this call was made.
     */
    uint32_t lineNumber;
    /**
     * The region which was called.
     */
    Region *enteredRegion;
    /**
    * The region which made the call.
    */
    Region *leftRegion;
};

/**
 * Map of the callsites.
 */
static std::map<OTF2_CallsiteRef, struct CallSite> callSiteMap;

/**
 * The callpath definition.
 */
struct CallPath {
    /**
     * The parent of this callpath.
     */
    CallPath *parent;
    /**
     * The region of this callpath.
     */
    Region *region;
};
/**
 * Map of the callpaths.
 */
static std::map<OTF2_CallpathRef, struct CallPath> callPathMap;

struct Group {
    /**
     * Name of this group.
     */
    std::string name;
    /**
     * The type of this group.
     */
    OTF2_GroupType groupType;
    /**
     * The paradigm of this communication group.
     */
    OTF2_Paradigm paradigm;
    /**
     * Flags for this group.
     */
    OTF2_GroupFlag groupFlags;
    /**
     * The number of members in this group.
     */
    uint32_t numberOfMembers;
    /**
     * The identifiers of the group members.
     */
    uint32_t members[];
};
/**
 * Map of the groups.
 */
static std::map<OTF2_GroupRef, struct Group> groupMap;

struct MetricMember {
    /**
     * Name of the metric.
     */
    std::string name;
    /**
     * Description of the metric.
     */
    std::string description;
    /**
     * Metric type: PAPI, etc.
     */
    OTF2_MetricType metricType;
    /**
     * Metric mode: accumulative, fix, relative, etc.
     */
    OTF2_MetricMode metricMode;
    /**
     * Type of the value. Only OTF2_TYPE_INT64, OTF2_TYPE_UINT64, and OTF2_TYPE_DOUBLE are valid types. If this metric member is recorded in a Metric event, than this type and the type in the event must match.
     */
    OTF2_Type valueType;
    /**
     * The recorded values should be handled in this given base, either binary or decimal. This information can be used if the value needs to be scaled.
     */
    OTF2_Base base;
    /**
     * The values inside the Metric events should be scaled by the factor base^exponent, to get the value in its base unit. For example, if the metric values come in as KiBi, than the base should be OTF2_BASE_BINARY and the exponent 10. Than the writer does not need to scale the values up to bytes, but can directly write the KiBi values into the Metric event. At reading time, the reader can apply the scaling factor to get the value in its base unit, ie. in bytes.
     */
    int64_t exponent;
    /**
     * Unit of the metric. This needs to be the scale free base unit, ie. "bytes", "operations", or "seconds". In particular this unit should not have any scale prefix.
     */
    std::string unit;
};
/**
 * Map of the metric members.
 */
static std::map<OTF2_MetricMemberRef, struct MetricMember> metricMemberMap;
/**
 * This is a polymorphic definition class.
 */
struct Metric {
};

/**
 * Map of the metrics.
 */
static std::map<OTF2_MetricRef, struct Metric> metricMap;
/**
 * For a metric class it is implicitly given that the event stream that records the metric is also the scope. A metric class can contain multiple different metrics.
 */
struct MetricClass : Metric {
    /**
     * Define occurences of a metric set.
     */
    OTF2_MetricOccurrence metricOccurrence;
    /**
     * What kind of locations will record this metric class, or will this metric class only be recorded by metric instances.
     */
    OTF2_RecorderKind recorderKind;
    /**
     * Number of metrics within the set.
     */
    uint8_t numberOfMetrics;
    /**
     * List of metric members.
     */
    MetricMember *metricMembers[];
};
/**
 * Map of the metric classes.
 */
static std::map<OTF2_MetricRef, struct MetricClass> metricClassMap;

/**
 * A metric instance is used to define metrics that are recorded at one location for multiple locations or for another location. The occurrence of a metric instance is implicitly of type OTF2_METRIC_ASYNCHRONOUS.
 */
struct MetricInstance : Metric {
    /**
     * The instanced MetricClass. This metric class must be of kind OTF2_RECORDER_KIND_ABSTRACT.
     */
    MetricClass *metricClass;
    // TODO Does this work ? It should be Metric according to the loc
    /**
     * Recorder of the metric: LocationID.
     */
    Location *recorder;
    /**
     * Defines type of scope: location, location group, system tree node, or a generic group of locations.
     */
    OTF2_MetricScope metricScope;
    /**
     * Scope of metric: ID of a location, location group, system tree node, or a generic group of locations.
     */
    uint64_t scope;
};
/**
 * Map of the metric instances.
 */
static std::map<OTF2_MetricRef, struct MetricInstance> metricInstanceMap;
/**
 * The communicator definition.
 */
struct Comm {
    /**
     * The name given by calling MPI_Comm_set_name on this communicator. Or the empty name to indicate that no name was given.
     */
    std::string name;
    /**
     * The describing MPI group of this MPI communicator
     *
     * The group needs to be of type OTF2_GROUP_TYPE_COMM_GROUP or OTF2_GROUP_TYPE_COMM_SELF. References a Group definition.
     */
    Group *group;
    /**
     * The parent MPI communicator from which this communicator was created, if any. Use OTF2_UNDEFINED_COMM to indicate no parent.
     */
    Comm *parent;
#if OTF2_VERSION_MAJOR >= 3
    /**
     * Special characteristics of this communicator.
     */
    OTF2_CommFlag flags;
#endif
};
/**
* Map of the comm.
*/
static std::map<OTF2_CommRef, struct Comm> commMap;

/**
 * The parameter definition.
 */
struct Parameter {
    /**
     * Name of the parameter (variable name etc.)
     */
    std::string name;
    /**
     * Type of the parameter, OTF2_ParameterType for possible types.
     */
    OTF2_ParameterType parameterType;
};
/**
* Map of the parameters.
*/
static std::map<OTF2_ParameterRef, struct Parameter> parameterMap;
/**
 * Macro to grab the value from the attribute.
 */
#define GET_VALUE_FROM_ATTRIBUTE(attribute, type) ((type == OTF2_TYPE_NONE) ? throw std::invalid_argument("Attribute had OTF2_TYPE_NONE type.") : \
    (type == OTF2_TYPE_UINT8) ? attribute.uint8 : \
    (type == OTF2_TYPE_UINT16) ? attribute.uint16 : \
    (type == OTF2_TYPE_UINT32) ? attribute.uint32 :\
    (type == OTF2_TYPE_UINT64) ? attribute.uint64 :\
    (type == OTF2_TYPE_INT8) ? attribute.int8 :\
    (type == OTF2_TYPE_INT16) ? attribute.int16 :\
    (type == OTF2_TYPE_INT32) ? attribute.int32 :\
    (type == OTF2_TYPE_INT64) ? attribute.int64 :\
    (type == OTF2_TYPE_FLOAT) ? attribute.float32 :\
    (type == OTF2_TYPE_DOUBLE) ? attribute.float64 :\
    (type == OTF2_TYPE_STRING) ? attribute.stringRef :\
    (type == OTF2_TYPE_ATTRIBUTE) ? attribute.attributeRef :\
    (type == OTF2_TYPE_LOCATION) ? attribute.locationRef :\
    (type == OTF2_TYPE_REGION) ? attribute.regionRef :\
    (type == OTF2_TYPE_GROUP) ? attribute.groupRef :\
    (type == OTF2_TYPE_METRIC) ? attribute.metricRef :\
    (type == OTF2_TYPE_COMM) ? attribute.commRef :\
    (type == OTF2_TYPE_PARAMETER) ? attribute.parameterRef :\
    (type == OTF2_TYPE_RMA_WIN) ? attribute.rmaWinRef :\
    (type == OTF2_TYPE_SOURCE_CODE_LOCATION) ? attribute.sourceCodeLocationRef :\
    (type == OTF2_TYPE_CALLING_CONTEXT) ? attribute.callingContextRef :\
    (type == OTF2_TYPE_INTERRUPT_GENERATOR) ? attribute.interruptGeneratorRef :\
    (type == OTF2_TYPE_IO_FILE) ? attribute.ioFileRef :\
    (type == OTF2_TYPE_IO_HANDLE) ? attribute.ioHandleRef :\
    throw std::invalid_argument("Couldn't match type"))

/**
 * A window defines the communication context for any remote-memory access operation.
 */
struct RmaWin {
    /**
     * Name, e.g. 'GASPI Queue 1', 'NVidia Card 2', etc..
     */
    std::string name;
    /**
     * Communicator object used to create the window.
     */
    Comm *comm;
#if OTF2_VERSION_MAJOR >= 3
    /**
     * Special characteristics of this RMA window.
     */
     OTF2_RmaWinFlag flags;
#endif
};

/**
* Map of the RMA Windows.
*/
static std::map<OTF2_RmaWinRef, struct RmaWin> rmaWinMap;

/**
 * The metric class recorder definition.
 */
struct MetricClassRecorder {
    /**
     * Parent MetricClass, or MetricInstance definition to which this one is a supplementary definition. References a MetricClass, or a MetricInstance definition.
     */
    Metric *metric;
    /**
     * The location which recorded the referenced metric class.
     */
    Location *recorder;
};

/**
 * An arbitrary key/value property for a SystemTreeNode definition.
 */
struct SystemTreeNodeProperty {
    /**
     * Parent SystemTreeNode definition to which this one is a supplementary definition.
     */
    SystemTreeNode *systemTreeNode;
    /**
     * Name of the property.
     */
    std::string name;
    /**
     * The type of the property.
     */
    OTF2_Type type;
    /**
     * The value of the property.
     */
    OTF2_AttributeValue value;
};
/**
 * The system tree node domain definition.
 */
struct SystemTreeNodeDomain {
    /**
     * Parent SystemTreeNode definition to which this one is a supplementary definition.
     */
    SystemTreeNode *systemTreeNode;
    /**
     * The domain in which the referenced SystemTreeNode operates in.
     */
    OTF2_SystemTreeDomain systemTreeDomain;
};

/**
 * An arbitrary key/value property for a LocationGroup definition.
 */
struct LocationGroupProperty {
    /**
     * Parent LocationGroup definition to which this one is a supplementary definition.
     */
    LocationGroup *locationGroup;
    /**
     * Name of the property.
     */
    std::string name;
    /**
     * The type of the property.
     */
    OTF2_Type type;
    /**
     * Value of the property.
     */
    OTF2_AttributeValue value;
};

/**
 * An arbitrary key/value property for a Location definition.
 */
struct LocationProperty {
    /**
     * Parent Location definition to which this one is a supplementary definition.
     */
    LocationGroup *locationGroup;
    /**
     * Name of the property.
     */
    std::string name;
    /**
     * The type of the property.
     */
    OTF2_Type type;
    /**
     * Value of the property.
     */
    OTF2_AttributeValue value;
};

/**
 * Each dimension in a Cartesian topology is composed of a global id, a name, its size, and whether it is periodic or not.
 */
struct CartDimension {
    /**
     * The name of the cartesian topology dimension.
     */
    std::string name;
    /**
     * The size of the cartesian topology dimension.
     */
    uint32_t size;
    /**
     * Periodicity of the cartesian topology dimension.
     */
    OTF2_CartPeriodicity cartPeriodicity;
};
/**
 * Map of the cartDimensions.
 */
static std::map<OTF2_CartDimensionRef, struct CartDimension> cartDimensionMap;

/**
 * Each topology is described by a global id, a reference to its name, a reference to a communicator, the number of dimensions, and references to those dimensions. The topology type is defined by the paradigm of the group referenced by the associated communicator.
 */
struct CartTopology {
    /**
     * The name of the topology.
     */
    std::string name;
    /**
     * Communicator object used to create the topology.
     */
    Comm *communicator;
    /**
     * Number of dimensions.
     */
    uint8_t numberOfDimensions;
    /**
     * The dimensions of the topology.
     */
    CartDimension *cartDimensions[];
};
/**
* Map of cart topologies.
*/
static std::map<OTF2_CartTopologyRef, struct CartTopology> cartTopologyMap;

/**
 * Defines the coordinate of the location referenced by the given rank (w.r.t. the communicator associated to the topology) in the referenced topology.
 */
struct CartCoordinate {
    /**
     * Parent CartTopology definition to which this one is a supplementary definition.
     */
    CartTopology *cartTopology;
    /**
     * The rank w.r.t. the communicator associated to the topology referencing this coordinate.
     */
    uint32_t rank;
    /**
     * Number of dimensions.
     */
    uint8_t numberOfDimensions;
    /**
     * Coordinates, indexed by dimensions.
     */
    uint8_t coordinates[];
};
/**
 * The definition of a source code location as tuple of the corresponding file name and line number.
 *
 * When used to attach source code annotations to events, use the OTF2_AttributeList with a Attribute definition named "SOURCE_CODE_LOCATION" and typed OTF2_TYPE_SOURCE_CODE_LOCATION.
 */
struct SourceCodeLocation {
    /**
     * The name of the file for the source code location.
     */
    std::string file;
    /**
     * The line number for the source code location.
     */
    uint32_t lineNumber;
};
/**
* Map of source code locations.
*/
static std::map<OTF2_SourceCodeLocationRef, struct SourceCodeLocation> sourceCodeLocationMap;

/**
 * Defines a node in the calling context tree. These nodes are referenced in the CallingContextSample, CallingContextEnter, and CallingContextLeave events.
 *
 * The referenced CallingContext node in these events form a path which represents the calling context at this time. This path will be partitioned into at most three sub-paths by the unwindDistance attribute. For the CallingContextLeave event, the unwindDistance is defined to be 1.
 *
 * Starting from the referenced CallingContext node, the first N >= 0 nodes were newly entered regions since the previous calling context event. The next node is a region which was not left but made progress since the previous calling context event. All other nodes did not make progress at all, and thus the regions were neither left nor entered again. The unwindDistance is then N+1. In case the unwindDistance is 0, there are neither newly entered regions nor regions which made progress.
 *
 * It is guaranteed, that the node referenced by the unwindDistance exists in the previous and current calling context. All descendants of this node's child in the previous calling context were left since the previous calling context event.
 *
 * It is valid that this node is the OTF2_UNDEFINED_CALLING_CONTEXT node and that this node is already reached after unwindDistance $ - 1 $ steps. In the latter case, there exists no region which made progress, all regions in the previous calling context were left and all regions in the current calling context were newly entered.
 *
 * Note that for CallingContextLeave events, the parent of the referenced CallingContext must be used as the previous calling context for the next event.
 *
 * Regions which were entered with a CallingContextEnter event form an upper bound for the unwind distance, i.e., the unwindDistance points either to the parent of the last such entered region, or a node which is a descendant to this parent.
 *
 * To summarize, an unwindDistance of $ 0 $ means that no regions were left, newly entered, or made any progress. An unwindDistance of $ 1 $ means that some regions were left regarding the previous calling context, no regions were newly entered, and there was progress in the region of the first node. An unwindDistance greater than $ 1 $ means that some regions were left regarding the previous calling context, there was progress in one region, and the first unwindDistance -1 regions were newly entered.
 */
struct CallingContext {
    /**
     * The region.
     */
    Region *region;
    /**
     * The absolute source code location of this calling context.
     */
    SourceCodeLocation *sourceCodeLocation;
    /**
     * Parent of this context.
     */
    CallingContext *parent;
};
/**
* Map of callingContexts.
*/
static std::map<OTF2_CallingContextRef, struct CallingContext> callingContextMap;

/**
 * An arbitrary key/value property for a CallingContext definition.
 */
struct CallingContextProperty {
    /**
     * Parent CallingContext definition to which this one is a supplementary definition.
     */
    CallingContext *callingContext;
    /**
     * Property name.
     */
    std::string name;
    /**
     * The type of this property. Must match with the defined type of the property.
     */
    OTF2_Type type;
    /**
     * The value of this property.
     */
    OTF2_AttributeValue value;
};
/**
 * Defines an interrupt generator which periodically triggers CallingContextSample events. If the mode of the interrupt generator is set to OTF2_INTERRUPT_GENERATOR_MODE_TIME, the generator produces interrupts which are uniformly distributed over time, and the unit of the period is implicitly in seconds. If the mode is OTF2_INTERRUPT_GENERATOR_MODE_COUNT, the interrupt is triggered if a specific counter threshold is reached in the system. Therefore these samples are unlikely to be uniformly distributed over time. The unit of the period is then implicitly a number (threshold value).
 *
 * The interrupts period in base unit (which is implicitly seconds or number, based on the mode) is derived out of the base, the exponent, and the period attributes by this formula:
 *
 * base-period = period x base^exponent
 */
struct InterruptGenerator {
    /**
     * The name of this interrupt generator.
     */
    std::string name;
    /**
     * Mode of the interrupt generator;
     */
    OTF2_InterruptGeneratorMode interruptGeneratorMode;
    /**
     * The base for the period calculation.
     */
    OTF2_Base base;
    /**
     * The exponent for the period calculation.
     */
    int64_t exponent;
    /**
     * The period this interrupt generator generates interrupts.
     */
    uint64_t period;
};
/**
* Map of the InterruptGenerators.
*/
static std::map<OTF2_InterruptGeneratorRef, struct InterruptGenerator> interruptGeneratorMap;

/**
 *  This is a polymorphic definition class.
 */
struct IoFile {
};
/**
* Map of the IoFiles.
*/
static std::map<OTF2_IoFileRef, struct IoFile> ioFileMap;

/**
 * Defines a regular file from which an IoHandle can be created.
 *
 * This definition is member of the polymorphic IoFile definition class. All definitions of this polymorphic definition class share the same global identifier namespace.
 */
struct IoRegularFile : IoFile {
    /**
     * Name of the file.
     */
    std::string name;
    /**
     * Defines the physical scope of this IoRegularFile in the system tree. E.g., two IoRegularFile definitions with the same name but different scope values are physically different, thus I/O operations through IoHandles do not operate on the same file.
     */
    SystemTreeNode *scope;
};

/**
 * Defines a directory from which an IoHandle can be created.
 *
 * This definition is member of the polymorphic IoFile definition class. All definitions of this polymorphic definition class share the same global identifier namespace.
 */
struct IoDirectory : IoFile {
    /**
     * Name of the directory.
     */
    std::string name;
    /**
     * Defines the physical scope of this IoDirectory in the system tree. E.g., two IoDirectory definitions with the same name but different scope values are physically different, thus I/O operations through IoHandles do not operate on the same directory.
     */
    SystemTreeNode *scope;
};

/**
 * Extensible annotation for the polymorphic IoFile definition class.
 * The tuple (ioFile, name) must be unique.
 */
struct IoFileProperty {
    /**
     * Parent IoRegularFile definition to which this one is a supplementary definition. References a IoRegularFile definition.
     */
    IoFile *ioFile;
    //TODO Check if you can put a IORegularFile here
    /**
     * Property name.
     */
    std::string name;
    /**
     * The type of this property.
     */
    OTF2_Type type;
    /**
     * The value of this property.
     */
    OTF2_AttributeValue value;
};
/**
 * Defines an I/O handle which will be used by subsequent I/O operations. I/O operations can only be applied to active I/O handles. An I/O handle gets active either if it was marked with the OTF2_IO_HANDLE_FLAG_PRE_CREATED flag, after it was referenced in an IoCreateHandle event, or it was referenced in the newHandle attribute of an IoDuplicateHandle event. It gets inactive if it was referenced in an IoDestroyHandle event. This life cycle can be repeated indefinitely. Though the OTF2_IO_HANDLE_FLAG_PRE_CREATED flag is unset after a IoDuplicateHandle event. All Locations of a LocationGroup have access to an active IoHandle, regardless which Location of the LocationGroup activated the IoHandle.
 */
struct IoHandle {
    /**
     * Handle name.
     */
    std::string name;
    /**
     * File identifier.
     */
    IoFile *file;
    /**
     * The I/O paradigm.
     */
    IoParadigm *ioParadigm;
    /**
     * Special characteristics of this handle.
     */
    OTF2_IoHandleFlag ioHandleFlags;
    /**
     * Scope of the file handle. This scope defines which process can access this file via this handle and also defines the collective context for this handle. References a Comm definition.
     */
    Comm *comm;
    /**
     * Parent, in case this I/O handle was created and operated by an higher- level I/O paradigm.
     */
    IoHandle *parent;
};
/**
* Map of the IoHandles.
*/
static std::map<OTF2_IoHandleRef, struct IoHandle> ioHandleMap;

/**
 * Provide the I/O access mode and status flags for pre-created IoHandles.
 *
 * Only allowed once for a IoHandle definition with the OTF2_IO_HANDLE_FLAG_PRE_CREATED flag set.
 */
struct IoPreCreatedHandleState {
    /**
     * Parent IoHandle definition to which this one is a supplementary definition.
     */
    IoHandle *ioHandle;
    /**
     * The access mode of the pre-created IoHandle.
     */
    OTF2_IoAccessMode mode;
    /**
     * The status flags of the pre-created Io-Handle.
     */
    OTF2_IoStatusFlag statusFlags;
};

/**
 *  A parameter for a callpath definition.
 */
struct CallpathParameter {
    /**
     * Parent Callpath definition to which this one is a supplementary definition.
     */
    CallPath *callpath;
    /**
     * The parameter of this callpath.
     */
    Parameter *parameter;
    /**
     * The type of the attribute value. Must match the type of the parameter.
     */
    OTF2_Type type;
    /**
     * The value of the parameter for this callpath.
     */
    OTF2_AttributeValue value;
};
