/*
 * This software may be modified and distributed under the terms of
 * a BSD-style license.  See the COPYING file in the package base
 * directory for details.
 *
 */
#include <otf2/otf2.h>
#include <otf2/OTF2_GeneralDefinitions.h>
#include "static_callbacks.h"
#include <iterator>
#include <iostream>
#include <algorithm>
#include <unistd.h>
#include <cstring>
#include <chrono>
#include <iomanip>

#define SET_GLOBAL_DEF_CALLBACK(CALL_NAME) OTF2_GlobalDefReaderCallbacks_Set##CALL_NAME##Callback(global_def_callbacks, &DefReader##CALL_NAME)
#define SET_GLOBAL_EVT_CALLBACK(CALL_NAME) OTF2_GlobalEvtReaderCallbacks_Set##CALL_NAME##Callback(event_callbacks, &EvtReader##CALL_NAME)

OTF2_GlobalDefReader *GlobalDefReader_setCallbacks(OTF2_Reader *reader, void *userData) {
    // Get the global definition reader
    OTF2_GlobalDefReader *global_def_reader = OTF2_Reader_GetGlobalDefReader(reader);
    // Register the above defined global definition callbacks.
    // All other definition callbacks will be deactivated.
    // And instruct the reader to pass the locations object to each call of the callbacks.
    OTF2_GlobalDefReaderCallbacks *global_def_callbacks = OTF2_GlobalDefReaderCallbacks_New();
    DBG_PRINT("Setting custom callbacks for Global DefReader");

    // Set Custom Callbacks
    SET_GLOBAL_DEF_CALLBACK(String);
    SET_GLOBAL_DEF_CALLBACK(SystemTreeNode);
    SET_GLOBAL_DEF_CALLBACK(LocationGroup);
    SET_GLOBAL_DEF_CALLBACK(Location);
    SET_GLOBAL_DEF_CALLBACK(Region);
    SET_GLOBAL_DEF_CALLBACK(Attribute);

    OTF2_Reader_RegisterGlobalDefCallbacks(reader, global_def_reader, global_def_callbacks, userData);
    OTF2_GlobalDefReaderCallbacks_Delete(global_def_callbacks);
    return global_def_reader;
}

OTF2_GlobalEvtReader *GlobalEvtReader_setCallbacks(OTF2_Reader *reader, void *userData) {
    // Open a new global event reader.
    // This global reader automatically contains all previously opened local event readers.
    OTF2_GlobalEvtReader *global_evt_reader = OTF2_Reader_GetGlobalEvtReader(reader);
    OTF2_GlobalEvtReaderCallbacks *event_callbacks = OTF2_GlobalEvtReaderCallbacks_New();
    // Register the above defined global event callbacks. All other global event callbacks will be deactivated.

    // Test Callbacks, Not used anymore
    SET_GLOBAL_EVT_CALLBACK(Enter);
    SET_GLOBAL_EVT_CALLBACK(Leave);

    // And Register those callbacks
    OTF2_Reader_RegisterGlobalEvtCallbacks(reader, global_evt_reader, event_callbacks, userData);
    OTF2_GlobalEvtReaderCallbacks_Delete(event_callbacks);
    return global_evt_reader;
}

void show_usage(int argc, char**argv) {
  std::cout<<"Usage: "<<argv[0]<<" [option] trace_file.otf2"<<std::endl;
  std::cout<<"\t-v\tFull verbose mode"<<std::endl;
  std::cout<<"\t-vDef\tVerbose mode (definitions only)"<<std::endl;
  std::cout<<"\t-vEvt\tVerbose mode (events only)"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"\t-f\tPrint functions duration"<<std::endl;
  std::cout<<"\t-g\tOnly print global durations"<<std::endl;
  std::cout<<"\t-h\tShow help"<<std::endl;
}

int main(int argc, char **argv) {
    std::ofstream nullStream;
    std::ofstream tmp_def_stream;
    std::ofstream tmp_evt_stream;
    std::ofstream tmp_dgb_stream;
    nullStream.open("/dev/null");
    // Setting the good streams
    def_stream = &nullStream;
    evt_stream = &nullStream;
    dbg_stream = &nullStream;

    bool print_functions = false;
    bool print_global = false;

    int nb_opts = 0;
    for (int i = 1; i < argc; i++) {
      if (!strcmp(argv[i], "-v")) {
	std::cout << "Verbose using std::cout for everything" << std::endl;
	def_stream = &std::cout;
	dbg_stream = &std::cout;
	evt_stream = &std::cout;
	nb_opts++;
      } else if(!strcmp(argv[i], "-vDef")) {
	std::cout << "Verbose using std::cout for Def" << std::endl;
	def_stream = &std::cout;
	nb_opts++;
      } else if(!strcmp(argv[i], "-vEvt")) {
	std::cout << "Verbose using std::cout for Evt" << std::endl;
	evt_stream = &std::cout;
	nb_opts++;
      } else if(!strcmp(argv[i], "-f")) {
	std::cout << "Print function: true" << std::endl;
	print_functions = true;
	nb_opts++;
      } else if(!strcmp(argv[i], "-g")) {
	std::cout << "Print global: true" << std::endl;
	print_global = true;
	nb_opts++;
      } else if(!strcmp(argv[i], "-h")) {
	show_usage(argc, argv);
	return EXIT_SUCCESS;
      }
    } 

    if( argc <= nb_opts+1) {
	show_usage(argc, argv);
	return EXIT_SUCCESS;
    }

    // Setting the OTF2 File
    std::string otf2_file = argv[nb_opts + 1];
    // Check if the first arg is a valid otf2 file
    if (! (access(otf2_file.c_str(), F_OK) == 0 && strcmp(otf2_file.c_str() + (otf2_file.length() - 5), ".otf2") == 0)) {
      show_usage(argc, argv);
      return EXIT_SUCCESS;
    }

    if (!otf2_file.empty()) {
      DBG_PRINT("Custom OTF2 file provided: " << otf2_file);
    }

    if (otf2_file.empty()) {
#ifdef DEBUG
        otf2_file = "trace/example_trace/eztrace_log.otf2";
        DBG_PRINT("No OTF2 file provided, using fallback:" << otf2_file);
#else
        std::cerr << "No OTF2 file provided. "
                     "Use -i, -trace, -input or provide the path to the .otf2 file as the first argument."
                  << std::endl;
        return ENOENT;
#endif
    }

    /* Starting processing of trace */
    std::cout<<"[otf2-profiler] Starting profiling ..."<<std::endl;
    auto start = std::chrono::high_resolution_clock::now();

    auto global_begin = std::chrono::steady_clock::now();

    OTF2_Reader *reader = OTF2_Reader_Open(otf2_file.c_str());
    OTF2_Reader_SetSerialCollectiveCallbacks(reader);

    // Get the number of locations = threads
    uint64_t number_of_locations;
    OTF2_Reader_GetNumberOfLocations(reader, &number_of_locations);
    std::vector<uint64_t> locations;
    auto userData = UserData{
            &locations, number_of_locations
    };

    auto global_def_reader = GlobalDefReader_setCallbacks(reader, &userData);

    DBG_PRINT("Beginning definition reading over " << number_of_locations << " locations.");
    // Read all global definitions. Every time a location definition is read, the previously registered callback is triggered.
    // In definitions_read the number of read definitions is returned.
    uint64_t definitions_read = 0;
    OTF2_Reader_ReadAllGlobalDefinitions(reader, global_def_reader, &definitions_read);

#ifdef DEBUG
    assert(locations.size() == number_of_locations);
#endif

    DBG_PRINT("Read " << definitions_read << " definitions over " << number_of_locations << " locations.");
    auto definition_time = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::steady_clock::now() - global_begin).count();
    DBG_PRINT("Definition reading done in " << definition_time << "ms.");

    // When the locations are selected the according event and definition files can be opened.
    // Note that the local definition files are optional, thus we need to remember the success nullStream this call.
    bool successful_open_def_files = OTF2_Reader_OpenDefFiles(reader) == OTF2_SUCCESS;
    OTF2_Reader_OpenEvtFiles(reader);

    // When the files are opened the event and definition reader handle can be requested.
    // In this example for all.
    // To apply mapping tables stored in the local definitions, the local definitions must be read.
    // Though the existence nullStream these files are optional.
    // The call to OTF2_Reader_GetEvtReader is mandatory, but the result is unused.
    DBG_PRINT("Creating DefReader and EvtReader for locations.");
    for (unsigned long location: locations) {
        if (successful_open_def_files) {
            OTF2_DefReader *def_reader = OTF2_Reader_GetDefReader(reader, location);
            if (def_reader) {
                uint64_t def_reads = 0;
                OTF2_Reader_ReadAllLocalDefinitions(reader, def_reader, &def_reads);
                OTF2_Reader_CloseDefReader(reader, def_reader);
            }
        }
    }
    // The definition files can now be closed, if it was successfully opened in the first place.
    if (successful_open_def_files) {
        OTF2_Reader_CloseDefFiles(reader);
    }

    auto evt_reading_begin = std::chrono::steady_clock::now();

    auto global_evt_reader = GlobalEvtReader_setCallbacks(reader, &userData);

    OTF2_EvtReaderCallbacks *local_event_callbacks = OTF2_EvtReaderCallbacks_New();
    OTF2_EvtReaderCallbacks_SetEnterCallback(local_event_callbacks, &local_EvtReaderEnter);
    OTF2_EvtReaderCallbacks_SetLeaveCallback(local_event_callbacks, &local_EvtReaderLeave);

    // Read all events in the OTF2 archive.
    // The events are automatically ordered by the time they occurred in the trace.
    // Every time an enter or leave event is read, the previously registered callbacks are triggered.
    // In events_read the number nullStream read events is returned.
    DBG_PRINT("Beginning event reading over " << number_of_locations << " locations.");
    uint64_t total_events_read = 0;

    // If the trace contains many threads, we have to read them one by one
    // Otherwise, OTF2 fails when opening too many files
    for (auto it = locations.begin() ; it != locations.end() ; ++ it) {
      //      printf("Read events for location %d\n", *it);
      OTF2_EvtReader* evt_reader =
	OTF2_Reader_GetEvtReader( reader, (*it));
      OTF2_Reader_SelectLocation(reader, (*it));

      OTF2_Reader_RegisterEvtCallbacks (reader, evt_reader, local_event_callbacks, &userData);

      uint64_t events_read = 0;
      //      OTF2_Reader_ReadAllLocalEvents(reader, evt_reader, &events_read);
      OTF2_Reader_ReadLocalEvents(reader, evt_reader, 1000, &events_read);
      total_events_read += events_read;
      OTF2_Reader_CloseEvtReader (reader, evt_reader);
      //      break;
    }

    DBG_PRINT("Read " << total_events_read << " events over " << number_of_locations << " locations.");
    auto reading_time = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::steady_clock::now() - evt_reading_begin).count();
    DBG_PRINT("Event reading done in " << reading_time << "ms.");
    DBG_PRINT("Showing information concerning each thread.");

    // Finding the different MPI Groups
    struct MPI_Data {
      /* Thread data for that MPI Process */
      struct ThreadData data = {};
      OTF2_LocationRef mpi_rank;
      std::vector<OTF2_LocationRef> threads;
      std::map<OTF2_LocationRef, struct ThreadData> thread_data;
    };

    std::vector<MPI_Data> mpi_data(locationGroupMap.size());
    for(int i =0; i<locationGroupMap.size(); i++) {
	MPI_Data* d = &mpi_data[i];
	d->mpi_rank = i;
	d->thread_data={};
	d->data = {};
    }
    
    int nb_mpi_ranks = 0;
    for (auto it = locationGroupMap.begin() ; it != locationGroupMap.end() ; ++ it) {
      auto lg = (*it).second;
      if(lg.groupType == OTF2_LOCATION_GROUP_TYPE_PROCESS) {

	nb_mpi_ranks++;
	MPI_Data* d = &mpi_data[lg.id];
	d->mpi_rank = lg.id;
	d->thread_data={};
	d->data = {};
      }
    }

    for (auto &it: userData.thread_info) {
        OTF2_LocationRef l = it.first;
        ThreadData threadData = it.second;
	auto &location = locationMap[l];
	MPI_Data* d = &mpi_data[location.locationGroup->id];
	d->threads.push_back(location.id);
	d->thread_data[location.id] = threadData;
	d->data += threadData;
	printf("thread %d (%s) -> rank %d\n", location.id, location.name, location.locationGroup->id);
    }

#define COLOR_BOLD  "\e[1m"
#define COLOR_OFF   "\e[m"

    // And then printing the summary
    std::cout << COLOR_BOLD "===== Summary =====" COLOR_OFF << '\n';
    if (nb_mpi_ranks == 1) {
      std::cout << COLOR_BOLD "Number of threads: " << userData.locations->size() << COLOR_OFF << std::endl;
    } else {
      std::cout << COLOR_BOLD "Number of MPI processes: " << mpi_data.size() << COLOR_OFF << std::endl;
    }
    auto globalThreadData = ThreadData{};
    const std::string prefix = (mpi_data.size() == 1) ? "" : "   ";

    for(int i =0; i<nb_mpi_ranks; i++) {
      MPI_Data& mpiData = mpi_data[i];
      if(!print_global) {
	std::cout << COLOR_BOLD "Information about MPI Process " << locationGroupMap[mpiData.mpi_rank].name << "(id: "<< mpiData.mpi_rank<<" )"<<COLOR_OFF ":\n";
	std::cout << "   - Number of threads: " << mpiData.threads.size() << '\n';
	mpiData.data.print();
	if(print_functions)
	  mpiData.data.printFunctions();
      }

      for(int j=0; j<mpiData.threads.size(); j++) {
	OTF2_LocationRef ref = mpiData.threads[j];
	std::cout << prefix << "Information about thread " << locationMap[ref].name << " (id: "<<ref<<"):\n";
	mpiData.thread_data[ref].print(prefix);

	if(print_functions)
	  mpiData.thread_data[ref].printFunctions(prefix);

      }
      globalThreadData += mpiData.data;
    }

    if(print_global) {
      std::cout << COLOR_BOLD "Global information:" COLOR_OFF "\n";
      globalThreadData.print();
      if(print_functions)
	globalThreadData.printFunctions();
    }


    // The global event reader can now be closed and the event files too.
    OTF2_Reader_CloseGlobalEvtReader(reader, global_evt_reader);
    OTF2_Reader_CloseEvtFiles(reader);
    // At the end, close the reader and exit.
    // All opened event and definition readers are closed automatically.
    OTF2_Reader_Close(reader);
    DBG_PRINT(
	      "Read " << definitions_read << " definitions and " << total_events_read << " events over " << number_of_locations
                    << " locations.");
    DBG_PRINT("Total time of execution: " << std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::steady_clock::now() - global_begin).count() << "ms.");

    /* Print program duration */
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start);
    std::cout<<std::endl<<"[otf2-profiler] *** Elapsed time: "<< std::setw(7)<<duration.count()<<" nanoseconds. ***"<<std::endl;
    return EXIT_SUCCESS;
}
