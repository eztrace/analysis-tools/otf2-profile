# otf2-profile


OTF2-Profile is a trace analysis tool. It reads an OTF2 trace and
generates a profile.

## Requirements
- [otf2](https://www.vi-hps.org/projects/score-p). [Download](https://perftools.pages.jsc.fz-juelich.de/cicd/otf2/)

## Building

First, make sure `otf2-config` is in your `PATH`, then run the following commands:

```
cmake . -DCMAKE_INSTALL_PREFIX=$INSTALLATION_DIR
make install
```

## Using OTF2-Profile

```
$ otf2_profile sample_traces/mpi_ring_trace/eztrace_log.otf2 
===== Summary =====
Number of MPI processes: 4
Information concerning MPI Process P#0T#0( 0 ):
   - Number of threads: 1
   - Time spent inside MPI Functions:   3,238 µs (100%)
   - Time spent inside Other Functions:     0 µs (0%)
Information concerning MPI Process P#1T#0( 536870911 ):
   - Number of threads: 1
   - Time spent inside MPI Functions:   10 ms (100%)
   - Time spent inside Other Functions:  0 ms (0%)
Information concerning MPI Process P#2T#0( 1073741822 ):
   - Number of threads: 1
   - Time spent inside MPI Functions:   10 ms (100%)
   - Time spent inside Other Functions:  0 ms (0%)
Information concerning MPI Process P#3T#0( 1610612733 ):
   - Number of threads: 1
   - Time spent inside MPI Functions:   10 ms (100%)
   - Time spent inside Other Functions:  0 ms (0%)
Global information:
   - Time spent inside MPI Functions:   34 ms (100%)
   - Time spent inside Other Functions:  0 ms (0%)
```

Several options can be passed to `otf2_profile`, including:
```
Usage: ./install/bin/otf2_profile [option] trace_file.otf2
        -v      Full verbose mode
        -vDef   Verbose mode (definitions only)
        -vEvt   Verbose mode (events only)

        -f      Print functions duration
        -g      Only print global durations
        -h      Show help
```

Example:
```
$ otf2_profile -f -g sample_traces/mpi_ring_trace/eztrace_log.otf2 
Print function: true
Print global: true
===== Summary =====
Number of MPI processes: 4
Global information:
   - Time spent inside MPI Functions:   34 ms (100%)
   - Time spent inside Other Functions:  0 ms (0%)
   - Summary of function calls 
    #Function                   nb_calls   tot_duration   min_duration   max_duration   avg_duration
     MPI_Barrier                      44     27,235 µs          0 µs      7,946 µs        618 µs
     MPI_Send                         40        100 µs          0 µs         15 µs          2 µs
     MPI_Recv                         40      6,927 µs          0 µs        447 µs        173 µs
```
## Contributing

Contribution to OTF2-Profile are welcome. Just send us a pull request.

## License
OTF2-Profile is distributed under the terms of both the BSD 3-Clause license.

See [LICENSE](LICENSE) for details
